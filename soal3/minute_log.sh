#!/bin/bash

user=$(whoami)
mkdir -p /home/$user/log 
fileout=metrics_$(date +"%Y%m%d%H%M%S").log

mem=$(free -m | grep Mem: | awk '{print $2","$3","$4","$5","$6","$7}')
swap=$(free -m | grep Swap: | awk '{print $2","$3","$4}')
path=$(du -sh /home/$user/ | awk '{print $2","$1}')

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > /home/$user/log/$fileout
echo "$mem,$swap,$path" >> /home/$user/log/$fileout
