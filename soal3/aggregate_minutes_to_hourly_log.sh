#!/bin/bash
user=$(whoami)
timenow=$(date +"%Y%m%d%H")
fileout=metrics_agg_$(date +"%Y%m%d%H").log

minMem=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{print $1","$2","$3","$4","$5","$6}' | sort | head -1)
maxMem=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{print $1","$2","$3","$4","$5","$6}' | sort | tail -1)
avgMem=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{total1+=$1;total2+=$2;total3+=$3;total4+=$4;total5+=$5;total6+=$6;count++} 
END{print(total1/count)","(total2/count)","(total3/count)","(total4/count)","(total5/count)","(total6/count)}')

minSwap=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{print $7","$8","$9}' | sort | head -1)
maxSwap=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{print $7","$8","$9}' | sort | tail -1)
avgSwap=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{total7+=$7;total8+=$8;total9+=$9;count++} 
END{print(total7/count)","(total8/count)","(total9/count)}')

minPath=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{print $11}' | sort | head -1)
maxPath=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{print $11}' | sort | tail -1)
avgPath=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{total11+=$11;count++} 
END {print total11/count}')


echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > /home/$user/log/$fileout
echo "minimum,$minMem,$minSwap,/home/$user/,$minPath" >> /home/$user/log/$fileout
echo "maximum,$maxMem,$maxSwap,/home/$user/,$maxPath" >> /home/$user/log/$fileout
echo "average,$avgMem,$avgSwap,/home/$user/,$avgPath" >> /home/$user/log/$fileout

# crontab -e
# 0 * * * *
