#!/bin/bash
echo "-----REGISTER-----"
if [ -e users ]
then
cd users
touch user.txt
cd ..
touch log.txt
else
mkdir users
cd users
touch user.txt
cd ..
touch log.txt
fi

echo "masukkan username yang akan dipakai"
read user
echo "masukkan sandi "
read -s sandi

if [ $sandi == $user ]
then
echo "Sandi tidak boleh sama dengan username"
elif [ $sandi == ${sandi,,} ]
then
echo "sandi harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
elif [ $sandi == ${sandi^^} ]
then
echo "sandi harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
elif [ ${#sandi} -lt 8 ]
then
echo "sandi minimal memiliki 8 karakter"
elif ! [[ "$sandi"  =~ [0-9] ]]
then
echo "sandi harus disertai karater numerik"
elif grep -q $user "users/user.txt"; 
then
echo $(date +%D) $(date +%r) REGISTER:ERROR User already exist >> log.txt
else 
echo $user$sandi >> users/user.txt 
echo $(date +%D) $(date +%r) REGISTER:INFO User $user registered succesfully >> log.txt
fi


