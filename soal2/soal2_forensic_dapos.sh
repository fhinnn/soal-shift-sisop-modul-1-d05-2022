#!/bin/bash
# soal a
mkdir -p ./forensic_log_website_daffainfo_log


# soal b
touch ratarata.txt

awk 'BEGIN{FS=":"}
{   
    if(NR>1){
        if(NR==2){
            jam[0]=$3;
            count=1;
            n=1;
        }
        else{
            for(i=0;i<n;i++){
                if($3==jam[i]){
                    count+=1;
      	            break;
                }
            }
            if(i==n){
                jam[i]=$3;
                count+=1;
                n+=1;
            }
        }
    }
}END{print "Rata-rata serangan adalah sebanyak " count/(n-1) " requests per jam"}' log_website_daffainfo.log > ratarata.txt

mv ratarata.txt ./forensic_log_website_daffainfo_log


# soal c
touch result.txt

awk 'BEGIN{FS=":"}
{
    if(NR>1){
        if(NR==2){
        ip[0]=$1;
        count[0]=1;
        n=1;
    }
    else{
        for(i=0;i<n;i++){
            if($1==ip[i]){
                count[i]+=1;
      	        break;
            }
        }   
        if(i==n){
            ip[i]=$1;
            count[i]=1;
            n+=1;
        }
    }
}
}END{
max=0;j=0; 
for(i=0;i<n;i++){;
    if(count[i]>max){
        max=count[i];
        j=i;
    }
}print "IP yang paling banyak mengakses server adalah: " ip[j] " sebanyak " max " requests\n" }' log_website_daffainfo.log > result.txt


# soal d
awk 'BEGIN{FS=":"}
{if(NR>1){
    if($9 ~ /curl/) n++;
}
}END{print "Ada " n " requests yang menggunakan curl sebagai user-agent\n"}' log_website_daffainfo.log >> result.txt


# soal e
awk 'BEGIN{FS=":"}
{if(NR>1){
    if($2 ~ /23/ && $3 ~ /02/) print $1 "\n";
}
}END{}' log_website_daffainfo.log >> result.txt

mv result.txt ./forensic_log_website_daffainfo_log
