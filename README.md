## Penyelesaian Soal Shift 1 Sistem Operasi

## Kelompok D05

| Nama | NRP |
| ------ | ------ |
| DHAFIN ALMAS NUSANTARA | 5025201064 |
| NEISA HIBATILLAH ALIF | 5025201170 |
| FEBERLIZER EDNAR WILLIAM GULTOM|05111940007004|

## ---SOAL 1---
Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.

## Soal a
Han membuat sistem register pada script `register.sh` dan setiap user yang berhasil didaftarkan disimpan di dalam `file ./users/user.txt`. Han juga membuat sistem login yang dibuat di script `main.sh`

Code :
```
#!/bin/bash
echo "-----REGISTER-----"
if [ -e users ]
then
cd users
touch user.txt
cd ..
touch log.txt
else
mkdir users
cd users
touch user.txt
cd ..
touch log.txt
fi

echo "masukkan username yang akan dipakai"
read user
echo "masukkan sandi "
read -s sandi

echo $user$sandi >> users/user.txt 
```
Penjelasan :
- `-s` untuk menyembunyikan inputan user

## Soal b
Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumeric
- Tidak boleh sama dengan username

Code :
```
if [ $sandi == $user ]
    then
    echo "Sandi tidak boleh sama dengan username"
elif [ $sandi == ${sandi,,} ]
    then
    echo "sandi harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
elif [ $sandi == ${sandi^^} ]
    then
    echo "sandi harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
elif [ ${#sandi} -lt 8 ]
    then
    echo "sandi minimal memiliki 8 karakter"
elif ! [[ "$sandi"  =~ [0-9] ]]
    then
    echo "sandi harus disertai karater numerik"
```
Penjelasan :

- `${sandi,,}` untuk lowercase string
- `${sandi^^}` untuk uppercase string

## Soal c
Setiap percobaan login dan register akan tercatat pada log.txt dengan format : MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.

- Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists

- Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully

- Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME

- Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

Code :
```
elif grep -q $user "users/user.txt"; 
    then
    echo $(date +%D) $(date +%r) REGISTER:ERROR User already exist >> log.txt
else 
    echo $user$sandi >> users/user.txt 
    echo $(date +%D) $(date +%r) REGISTER:INFO User $user registered succesfully >> log.txt

```
```
if grep -q $user$sandi "users/user.txt"; 
    then
    echo $(date +%D) $(date +%r) LOGIN:INFO User $user logged in >> log.txt
else 
    echo $(date +%D) $(date +%r) LOGIN:ERROR Failed login atttempt on User $user >> log.txt

```
Penjelasan :
- `-grep -q` untuk mengecek apakah data ada di file user.txt 

## Soal d
Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :


- dl N ( N = Jumlah gambar yang akan didownload)
Untuk mendownload gambar dari `https://loremflickr.com/320/240` dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama `YYYY-MM-DD_USERNAME`. Gambar-gambar yang didownload juga memiliki format nama `PIC_XX`, dengan nomor yang berurutan (contoh : `PIC_01`, `PIC_02`, dst. ).  Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.

- att
Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.

Code :
```
echo "Masukkan Perintah !"
echo -n "perintah: "
read perintah

case $perintah in 
"dl")
    if [ -e $(date +%F)_$user.zip ]
        then
        read jumlah
        unzip -P $sandi $(date +%F)_$user.zip
        rm -r $(date +%F)_$user.zip
        cd $(date +%F)_$user
        ada=$(ls | wc -l)
            for ((x=$ada; x<=$jumlah+$ada; x=x+1))
                do 
                    if [ $x -lt 10 ]
                        then 
                        wget https://loremflickr.com/320/240 -O PIC_0$x
                    else
                         wget https://loremflickr.com/320/240 -O PIC_$x
                    fi

                 done
        cd ..
        zip --password $sandi -r $(date +%F)_$user.zip $(date +%F)_$user 
        rm -r $(date +%F)_$user
    else
        mkdir $(date +%F)_$user
        cd $(date +%F)_$user 
        read jumlah
            for ((x=1; x<=$jumlah; x=x+1))
                do 
                    if [ $x -lt 10 ]
                        then 
                        wget https://loremflickr.com/320/240 -O PIC_0$x
                    else
                        wget https://loremflickr.com/320/240 -O PIC_$x
                    fi

                done
        cd ..
        zip --password $sandi -r $(date +%F)_$user.zip $(date +%F)_$user 
        rm -r $(date +%F)_$user
    fi

```
Penjelasan :
- `-e` untuk mengecek apakah ada file tersebut
- `rm -r` untuk menghapus folder yang tidak kosong
- `wget` untk mendownload file dari link
- `ls | wc -l` untuk menghitung jumlah gambar yang ada dalam folder
- `zip --password` untuk menmbuat zip dengan enkripsi dari folder yang dipilih
- `unzip -P` untuk mengunzip zip yang tersandi tanpa menginput lagi passwordnya, hanya dengan memanggil sandi yang ada



## ---SOAL 2---

## Soal a
Buatlah folder terlebih dahulu bernama `forensic_log_website_daffainfo_log`.

Code:
```
mkdir -p ./forensic_log_website_daffainfo_log
```

Penjelasan: 
- `mkdir -p` digunakan untuk membuat folder pada terminal dan mengabaikannya jika sudah terdapat folder tersebut

## Soal b
Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama `ratarata.txt` ke dalam folder yang sudah dibuat sebelumnya.

Code:
```
touch ratarata.txt

awk 'BEGIN{FS=":"}
{   
    if(NR>1){
        if(NR==2){
            jam[0]=$3;
            count=1;
            n=1;
        }
        else{
            for(i=0;i<n;i++){
                if($3==jam[i]){
                    count+=1;
      	            break;
                }
            }
            if(i==n){
            	jam[i]=$3;
            	count+=1;
            	n+=1;
            }
    	}
     }
}END{
print "Rata-rata serangan adalah sebanyak " count/(n-1) " requests per jam"}' log_website_daffainfo.log > ratarata.txt

mv ratarata.txt ./forensic_log_website_daffainfo_log
```

Penjelasan: 
- `touch` digunakan untuk membuat file baru
- `FS=":"` berarti file menggunakan field separator berupa tanda ":"
- `NR` sendiri berarti line number pada file. Karena baris pertama pada file log_website_daffainfo.log berisi keterangan field, maka dapat diskip pengecekannya menggunakan `if(NR>1)`
- Bagian `if(NR==2)` berarti mengecek baris kedua kemudian memasukkan nilai jam pada array, kemudian menginisiasi jumlah keseluruhan `count` menjadi 1 dan jumlah rentang jam `n` menjadi 1 pula
- Pada bagian `if` dalam `for`, dilakukan pengecekan baris-baris berikutnya dengan nilai pada array yang sudah disimpan. Jika nilai jam pada baris baru sama dengan nilai yang sudah ada pada array, maka jumlah keseluruhan bertambah dan akan keluar dari looping. Jika tidak terpenuhi, maka nilai `i` bertambah sehingga keluar dari looping juga.
- Jika nilai `i` bertambah, maka otomatis nilainya akan sama dengan `n` yang berarti terdapat nilai jam yang baru pada baris tersebut. Maka nilai tersebut akan dimasukkan ke dalam array jam dan variabel `count` serta `n` akan bertambah juga.
- Karena jam terakhir berhenti tepat pada menit "00" dan detik "00", maka baris terakhir tidak akan dihitung sehingga pada `print` nilai `n` dikurangi 1.

Dokumentasi hasil pengerjaan:

![2b](https://gitlab.com/nha_14/dokumentasi-sisop-modul1/-/raw/main/img/1646560452540.jpg)

## Soal c
Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama `result.txt` kedalam folder yang sudah dibuat sebelumnya.

Code:
```
touch result.txt

awk 'BEGIN{FS=":"}
{
    if(NR>1){
        if(NR==2){
            ip[0]=$1;
            count[0]=1;
            n=1;
        }
        else{
            for(i=0;i<n;i++){
                if($1==ip[i]){
                    count[i]+=1;
      	            break;
                }
            }   
            if(i==n){
                ip[i]=$1;
                count[i]=1;
                n+=1;
            }
        }
    }
}END{
max=0;j=0; 
for(i=0;i<n;i++){;
    if(count[i]>max){
        max=count[i];
        j=i;
    }
}print "IP yang paling banyak mengakses server adalah: " ip[j] " sebanyak " max " requests\n" }' log_website_daffainfo.log > result.txt
```

Penjelasan: 
- Bagian `if(NR==2)` berarti mengecek baris kedua kemudian memasukkan nilai IP dan jumlah kemunculan IP tersebut pada array `ip` dan `count`, kemudian menginisiasi jumlah total IP `n` menjadi 1.
- Pada bagian `if` dalam `for`, dilakukan pengecekan baris-baris berikutnya dengan nilai pada array yang sudah disimpan. Jika nilai IP pada baris baru sama dengan nilai yang sudah ada pada array, maka jumlah kemunculan IP tersebut bertambah dan akan keluar dari looping. Jika tidak terpenuhi, maka nilai `i` bertambah sehingga keluar dari looping juga.
- Jika nilai `i` bertambah, maka otomatis nilainya akan sama dengan `n` yang berarti terdapat IP yang baru pada baris tersebut. Maka IP tersebut akan dimasukkan ke dalam array `ip`, jumlah kemunculan IP baru tersebut diinisiasi menjadi 1, dan `n` akan bertambah.
- Sebelum looping terakhir, diinisiasi jumlah IP yang paling banyak kemunculannya sebagai variabel `max` dan menyimpan indeks IP terbanyak dengan variabel `j`. Pada looping dilakukan pengecekan terhadap array yang berisi jumlah kemunculan IP.

## Soal d
Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl? Kemudian masukkan berapa banyak requestnya kedalam file bernama `result.txt` yang telah dibuat sebelumnya.

Code:
```
awk 'BEGIN{FS=":"}
{if(NR>1){
    if($9 ~ /curl/) n++;
}
}END{print "Ada " n " requests yang menggunakan curl sebagai user-agent\n"}' log_website_daffainfo.log >> result.txt
```

Penjelasan: 
- Karena yang dicek adalah field user-agent, maka yang dilihat adalah $9. Jika pada $9 terdapat substring "curl", maka variabel `n` atau total jumlah user-agent yang menggunakan curl ditambah

## Soal e
Pada jam 2 pagi pada tanggal 22 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama `result.txt` yang telah dibuat sebelumnya

Code:
```
awk 'BEGIN{FS=":"}
{if(NR>1){
    if($2 ~ /22/ && $3 ~ /02/) print $1 "\n";
}
}END{}' log_website_daffainfo.log >> result.txt

mv result.txt ./forensic_log_website_daffainfo_log
```

Penjelasan: 
- Karena yang dicek adalah field yang berisi tanggal dan jam, maka yang dilihat adalah $2 dan $3. Jika pada $2 terdapat substring "22" dan pada $3 terdapat substring "02", maka dicetak $1 atau informasi yang berisi IP.
- `mv` digunakan untuk memindahkan file ke dalam suatu directory

Dokumentasi hasil pengerjaan:

![2cde](https://gitlab.com/nha_14/dokumentasi-sisop-modul1/-/raw/main/img/1646560488802.jpg)
.
.
.

Kendala yang dialami:
- Pada soal b, terdapat sedikit keambiguan karena awalnya diasumsikan jika pada menit 00 dan detik 00 sudah termasuk rentang jam yang baru.



## ---SOAL 3---

## Soal a
Masukkan semua metrics ke dalam suatu file log bernama `metrics_{YmdHms}.log`. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah `metrics_20220131150000.log`.

Code:
```
user=$(whoami)
mkdir -p /home/$user/log 
fileout=metrics_$(date +"%Y%m%d%H%M%S").log

mem=$(free -m | grep Mem: | awk '{print $2","$3","$4","$5","$6","$7}')
swap=$(free -m | grep Swap: | awk '{print $2","$3","$4}')
path=$(du -sh /home/$user/ | awk '{print $2","$1}')

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > /home/$user/log/$fileout
echo "$mem,$swap,$path" >> /home/$user/log/$fileout
```

Penjelasan: 
- `whoami` digunakan untuk mengambil nama user.
- Dengan menggunakan `$(date +"%Y%m%d%H%M%S")`, nama file akan menyesuaikan sesuai dengan waktu ketika kode dijalankan.
- Untuk variabel `mem` dan `swap`, diambil output dari perintah `free -m`, kemudian perintah `grep` akan mengambil baris yang berisi kata `Mem:` untuk variabel `mem` dan `Swap:` untuk variabel `swap`. `awk` akan mengambil sesuai masing-masing jumlah kolomnya.
- Untuk variabel `path`, tinggal diambil kedua output dari perintah `du -sh /home/$user/`.

## Soal b
Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.

Penjelasan:
Untuk soal ini, perlu dibuat cron job. Di dalam `crontab -e`, dibuat syntax sebagai berikut sehingga file `minute_log.sh` akan dijalankan setiap menit.
```
* * * * * /home/{user}/minute_log.sh
```

Dokumentasi hasil pengerjaan:

![3ab1](https://gitlab.com/nha_14/dokumentasi-sisop-modul1/-/raw/main/img/1646560151172.jpg)
![3ab2](https://gitlab.com/nha_14/dokumentasi-sisop-modul1/-/raw/main/img/1646559898861.jpg)
![3ab3](https://gitlab.com/nha_14/dokumentasi-sisop-modul1/-/raw/main/img/1646560050130.jpg)
![3ab4](https://gitlab.com/nha_14/dokumentasi-sisop-modul1/-/raw/main/img/1646560108292.jpg)

## Soal c
Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi `metrics_agg_2022013115.log` dengan format `metrics_agg_{YmdH}.log`.

Code:
```
user=$(whoami)
timenow=$(date +"%Y%m%d%H")
fileout=metrics_agg_$(date +"%Y%m%d%H").log

minMem=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{print $1","$2","$3","$4","$5","$6}' | sort | head -1)
maxMem=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{print $1","$2","$3","$4","$5","$6}' | sort | tail -1)
avgMem=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{total1+=$1;total2+=$2;total3+=$3;total4+=$4;total5+=$5;total6+=$6;count++} 
END{print(total1/count)","(total2/count)","(total3/count)","(total4/count)","(total5/count)","(total6/count)}')

minSwap=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{print $7","$8","$9}' | sort | head -1)
maxSwap=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{print $7","$8","$9}' | sort | tail -1)
avgSwap=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{total7+=$7;total8+=$8;total9+=$9;count++} 
END{print(total7/count)","(total8/count)","(total9/count)}')

minPath=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{print $11}' | sort | head -1)
maxPath=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{print $11}' | sort | tail -1)
avgPath=$(cat /home/$user/log/metrics_$timenow*.log | grep home | awk -F "," '{total11+=$11;count++} 
END {print total11/count}')

echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > /home/$user/log/$fileout
echo "minimum,$minMem,$minSwap,/home/$user/,$minPath" >> /home/$user/log/$fileout
echo "maximum,$maxMem,$maxSwap,/home/$user/,$maxPath" >> /home/$user/log/$fileout
echo "average,$avgMem,$avgSwap,/home/$user/,$avgPath" >> /home/$user/log/$fileout
```

Penjelasan: 
- Tanda * pada path file setelah perintah `cat` digunakan untuk membaca file pada menit berapapun pada jam yang telah ditentukan.
- Perintah `grep` akan mengambil baris yang terdapat kata `home`, yaitu ada pada baris kedua tiap file.
- Untuk semua variabel `min`, akan diambl bagian teratas menggunakan `head -1` setelah diurutkan menggunakan perintah `sort` yang menaik (ascending).
- Untuk semua variabel `max`, akan diambl bagian terbawah menggunakan `tail -1` setelah diurutkan menggunakan perintah `sort` yang menaik (ascending).
- Untuk semua variabel `avg`, akan dihitung total isi tiap kolom `total` dan jumlah data yang dihitung `count`, kemudian hasilnya akan dioutput sebagai `total/count`.

Untuk membuat filenya berjalan tiap jam, perlu dibuat cron job. Di dalam `crontab -e`, syntax berikut digunakan sehingga file `aggregate_minutes_to_hourly_log.sh` akan dijalankan tiap jam pada menit ke-0.
```
0 * * * * /home/{user}/aggregate_minutes_to_hourly_log.sh
```

Dokumentasi hasil pengerjaan:

![3c1](https://gitlab.com/nha_14/dokumentasi-sisop-modul1/-/raw/main/img/1646560280613.jpg)
![3c2](https://gitlab.com/nha_14/dokumentasi-sisop-modul1/-/raw/main/img/16465602530051.jpg)
![3c3](https://gitlab.com/nha_14/dokumentasi-sisop-modul1/-/raw/main/img/1646560387595.jpg)

## Soal d
Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

Code:
```
chmod 400 /home/$user/log/$fileout
```
Penjelasan:
- `chmod 400` berfungsi agar user/owner hanya dapat membacanya (read-only) dan tidak dapat ditulis serta dieksekusi, sementara grup dan user lain tidak dapat membacanya dan tidak dapat ditulis serta dieksekusi.
- Potongan kode ini ditambahkan pada akhir file `minute_log.sh` dan `aggregate_minutes_to_hourly_log.sh`.

Dokumentasi hasil pengerjaan:

![3c1](https://gitlab.com/nha_14/dokumentasi-sisop-modul1/-/raw/main/img/1646560253005__1_.jpg)
